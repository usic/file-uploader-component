<?php

/**
 * Handle file uploads via XMLHttpRequest
 */
class qqUploadedFileXhr
{
	/**
	 * Save the file to the specified path
	 *
	 * @return boolean TRUE on success
	 */
	function save($path)
	{
		Yii::log('Enter save. Open php input', 'info');
		$input = fopen("php://input", "r");
		$temp = tmpfile();
		$realSize = stream_copy_to_stream($input, $temp);
		fclose($input);

		if ($realSize != $this->getSize()) {
			return false;
		}
		$target = fopen($path, "w");
		fseek($temp, 0, SEEK_SET);
		stream_copy_to_stream($temp, $target);
		fclose($target);

		return true;
	}

	/**
	 * @return mixed
	 */
	function getName()
	{
		return $_GET['qqfile'];
	}

	/**
	 * @return int
	 * @throws Exception
	 */
	function getSize()
	{
		if (isset($_SERVER["CONTENT_LENGTH"])) {
			return (int) $_SERVER["CONTENT_LENGTH"];
		} else {
			throw new Exception('Getting content length is not supported.');
		}
	}
}

/**
 * Handle file uploads via regular form post (uses the $_FILES array)
 */
class qqUploadedFileForm
{
	/**
	 * @param $data
	 *
	 * @return bool
	 */
	public function save($data)
	{
		$file = new FileStorage;
		$file->filename = $data['tmp_name'];
		$file->user = Yii::app()->user->login;
		$file->status = Files::STATUS_NOT_APPROVED;
		$file->mime = $data['mime'];
		$file->name = $data['name'];
		$file->setFile($data['tmp_name']);
		if($file->save())
			return $file;
		return false;
	}

	/**
	 * @return string
	 */
	function getName()
	{
		return $_FILES['qqfile']['name'];
	}

	/**
	 * @return integer
	 */
	function getSize()
	{
		return $_FILES['qqfile']['size'];
	}

	/**
	 * @return string
	 */
	function getTmpName()
	{
		return $_FILES['qqfile']['tmp_name'];
	}
}

class qqFileUploader
{
	/**
	 * @var array
	 */
	private $allowedMimeTypes = array();
	/**
	 * @var int
	 */
	private $sizeLimit = 10485760;
	/**
	 * @var object
	 */
	private $file;
	/**
	 * @var array
	 */
	private $data = array();

	/**
	 * @param array $allowedMimeTypes
	 * @param int $sizeLimit
	 */
	function __construct(array $allowedMimeTypes = array(), $sizeLimit = 10485760)
	{
		$this->allowedMimeTypes = $allowedMimeTypes;
		$this->sizeLimit = $sizeLimit;

		$this->checkServerSettings();
		if (isset($_GET['qqfile'])) {
			$this->file = new qqUploadedFileXhr();
		} elseif (isset($_FILES['qqfile'])) {
			$this->file = new qqUploadedFileForm();
		} else {
			$this->file = false;
		}
	}

	/**
	 * Check server settings
	 */
	private function checkServerSettings()
	{
		$postSize = $this->toBytes(ini_get('post_max_size'));
		$uploadSize = $this->toBytes(ini_get('upload_max_filesize'));

		if ($postSize < $this->sizeLimit || $uploadSize < $this->sizeLimit) {
			$size = max(1, $this->sizeLimit / 1024 / 1024) . 'M';
			die(json_encode(array('error' => 'increase post_max_size and upload_max_filesize to ' . $size)));
		}
	}

	/**
	 * @param $str
	 * @return int|string
	 */
	private function toBytes($str)
	{
		$val = trim($str);
		$last = strtolower($str[strlen($str) - 1]);
		switch ($last) {
			case 'g':
				$val *= 1024;
			case 'm':
				$val *= 1024;
			case 'k':
				$val *= 1024;
		}
		return $val;
	}

	/**
	 * Returns array('success'=>true) or array('error'=>'error message')
	 */
	function handleUpload()
	{
		$mime = explode(';', shell_exec("file -bi " . $this->file->getTmpName()));
		$this->data['mime'] = $mime[0];
		$this->data['name'] = $this->file->getName();
		$this->data['tmp_name'] = $this->file->getTmpName();

		if ($this->allowedMimeTypes !== '*') {
			if (!in_array($this->data['mime'], $this->allowedMimeTypes)) {
				return array(
					'error' => Yii::t(
							'app', 'Mime type {mime} is not allowed.', array('{mime}' => $this->data['mime'])
						)
				);
			}
		}
		$this->data['user'] = (Yii::app()->user->isGuest) ? 'guest' : Yii::app()->user->login;
		if (!$this->file) {
			return array('error' => Yii::t('app', 'No files were uploaded.'));
		}

		$size = $this->file->getSize();

		if ($size == 0) {
			return array('error' => Yii::t('app', 'File is empty'));
		}
		/* check file size */
		if ($size > $this->sizeLimit) {
			return array('error' => Yii::t('app', 'File is too large'));
		}

		/* save file */
		if ($f=$this->file->save($this->data)) {
			return array('success' => true, 'filename' => $this->file->getName(),'id'=>strval($f->_id));
		} else {
			return array(
				'error' => Yii::t(
						'app', 'Could not save uploaded file.' .
						'The upload was cancelled, or server error encountered'
					)
			);
		}
	}
}
